<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/','home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('disaster','DisasterController');
Route::resource('user','UserController');
Route::get('user/profile/{id}','UserController@profile')->name('user.profile');
Route::get('user/profile/{id}/update','UserController@edit_profile')->name('user.profile.update');

Route::get('map/{id}','HomeController@view_disaster')->name('view_disaster');
Route::get('map/prev/{id}','HomeController@prev_disaster')->name('view_prev_disaster');
Route::get('map/next/{id}','HomeController@next_disaster')->name('view_next_disaster');
Route::get('disaster/views/all','DisasterController@view_all_disaster')->name('view_all_disaster');

Route::get('demo','DisasterController@demo');
Route::get('search/disaster/','DisasterController@search')->name('search_all_disaster');
Route::get('search/user/','UserController@search')->name('search_all_users');