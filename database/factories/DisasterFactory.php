<?php

use Faker\Generator as Faker;
use App\Models\Disaster;
$factory->define(Disaster::class, function (Faker $faker) {
    return [
        'type' => $faker->creditCardType,
        'loc' => $faker->company,
        'lat' => $faker->latitude($min = -90, $max = 90),
        'lng' => $faker->longitude($min = -180, $max = 180)
    ];
});
