<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'firstname' => 'Min',
            'middlename' => '??',
            'lastname' => 'Valencia',
            'email' => 'admin@example.com',
            'password' => '123'
        ]);
    }
}
