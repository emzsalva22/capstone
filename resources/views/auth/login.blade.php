@extends('layouts.app')

@section('content')
<div class="flex items-center justify-center h-screen w-full">
    <div class="flex flex-wrap">
        <div class="w-full mb-4">
            <h1 class="text-grey-darker font-sans antialiased text-center">
                Disaster Risk Management System
            </h1>
        </div>
        <div class="w-4/5 py-8 mx-auto mt-4 max-w-sm border-2 border-grey shadow-md bg-white">
            <div class="w-full mb-8">
                <div id="box-title" class="text-center">
                    <h2 class="uppercase text-grey-darkest font-sans tracking-wide text-md">Welcome Back!</h2>
                </div>
            </div>
            <hr class="hr-border mb-8">
            <div class="w-full mb-4">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="w-full text-center">
                        <input id="email" type="email" class="w-4/5 h-8 px-2 border {{ $errors->has('email') ? 'border-red-dark' : 'border-grey-light' }}" name="email" value="{{ old('email') }}" placeholder="Email address" required autofocus>
                    </div>
                    <div class="w-full text-center mb-4">
                        {!! $errors->first('email', '<span class="text-red-dark text-sm mt-2">:message</span>') !!}
                    </div>
                    <div class="w-full text-center">
                        <input id="password" type="password" class="w-4/5 h-8 px-2 border {{ $errors->has('password') ? 'border-red-dark' : 'border-grey-light' }}" name="password" placeholder="*********" required>
                    </div>
                    <div class="w-full text-center  mb-4">
                        {!! $errors->first('password', '<span class="text-red-dark text-sm mt-2">:message</span>') !!}
                    </div>
                    <div class="w-full text-center">
                        <button type="submit" class="w-4/5 bg-blue hover:bg-blue-dark text-white text-sm font-semibold py-3 px-4">
                            Login
                        </button>
                    </div>
                </form>
            </div>  
            <hr class="hr-border mt-8">
            <div class="w-full text-center">
                <h1 class="text-xl mt-4">Powered by <span class="text-red-light">Group G</span></h1>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')

<style>
    .hr-border {
        border: .5px solid #DAE1E7 ;
    }
</style>
@endpush