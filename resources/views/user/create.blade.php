@extends('layouts.app')
@section('title','Edit User')
@section('content')
<div class="justify-center">
    <div class="w-1/4 md:mx-auto">
        <div class="text-center">
            <span class="text-grey-dark  text-lg">Create User</span>
        </div>
        <div class="bg-white shadow-lg rounded flex flex-col my-2">
            <form method="POST" action="{{ route('user.store') }}">
                {{ csrf_field() }} 
                @include('shared.alerts')
            <div class="px-8 pt-6 pb-8 mb-4 "> 
                <div class="-mx-3 mb-0 ">
                    <div class="px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="firstname">
                            Firstname
                        </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 "  id="firstname"  name="firstname" value="{{ old('firstname') }}">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="middlename">
                                middlename
                            </label>
                            <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 "  id="middlename"  name="middlename" value="{{ old('middlename') }}">
                        </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="lastname">
                                lastname
                            </label>
                            <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3"  id="lastname"  name="lastname" value="{{ old('lastname') }}">
                        </div>
                </div>
                <div class="-mx-3 mb-0 ">
                    <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email">
                                Email
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 "  id="email"  name="email" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                    <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="password">
                                Password
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 "  type="password" id="password"  name="password">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="password_confirmation">
                                    Confirm Password
                                </label>
                            <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 " type="password" id="password_confirmation"  name="password_confirmation">
                        </div>
                    </div>
                <div class="-mx-3 md:flex">
                    <div class="md:w-4/5 px-3 mb-0 md:mb-0">
                        <button href="{{ route('user.store') }}"class="inline-flex bg-green hover:bg-green-light text-white font-bold py-2 px-4 border-b-4 border-green-dark hover:border-blue rounded">
                            Create
                        </button>
                    </form>

                    </div>
                
                    <div class="md:w-auto px-3">
                        <a href="{{ route('user.index') }}"class="inline-flex bg-blue hover:bg-blue-light text-white font-bold py-2 px-4 border-b-4 border-blue-dark hover:border-blue rounded">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection