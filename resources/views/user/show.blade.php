@extends('layouts.app')
@section('title','Create User')
@section('content')
<div class="justify-center">
    <div class="w-1/4 md:mx-auto">
        <div class="text-center">
            <span class="text-grey-dark  text-lg">View User</span>
        </div>
        <div class="bg-white shadow-lg rounded flex flex-col my-2">
            <div class="px-8 pt-6 pb-8 mb-4 "> 
                <div class="-mx-3 mb-0 ">
                    <div class="px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="firstname">
                            Firstname
                        </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 cursor-not-allowed" disabled id="firstname"  name="firstname" value="{{ $user->firstname }}">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="middlename">
                                Middlename
                            </label>
                            <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 cursor-not-allowed" disabled id="middlename"  name="middlename" value="{{ $user->middlename }}">
                        </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="lastname">
                                Lastname
                            </label>
                            <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 cursor-not-allowed" disabled id="lastname"  name="lastname" value="{{ $user->lastname }}">
                        </div>
                </div>
                <div class="-mx-3 mb-0 ">
                    <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="email">
                                Email
                            </label>
                            <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3 cursor-not-allowed" disabled id="email"  name="email" value="{{ $user->email }}">
                        </div>
                </div>
                <div class="-mx-3 md:flex">
                    <div class="md:w-4/5 px-3 mb-0 md:mb-0">
                        <a href="{{ route('user.edit',$user->id) }}"class="inline-flex bg-green hover:bg-green-light text-white font-bold py-2 px-4 border-b-4 border-green-dark hover:border-blue rounded">
                            Update
                        </a>
                    </div>
                    <div class="md:w-4/5 px-3 mb-0 md:mb-0">
                        <a href="{{ route('user.destroy',$user->id) }}"class="inline-flex bg-red hover:bg-red-light text-white font-bold py-2 px-4 border-b-4 border-red-dark hover:border-red rounded" 
                            onclick="event.preventDefault();
                                document.getElementById('destroy-form').submit();">
                            Delete
                        </a>
                        <form id="destroy-form" action="{{ route('user.destroy',$user->id) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE')}}
                        </form>
                    </div>
                    <div class="md:w-auto px-3">
                        <a href="{{ route('user.index') }}"class="inline-flex bg-blue hover:bg-blue-light text-white font-bold py-2 px-4 border-b-4 border-blue-dark hover:border-blue rounded">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection