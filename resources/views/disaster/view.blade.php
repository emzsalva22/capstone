@extends('layouts.app')
@section('title','List All Disaster')
@section('content')
<div class="justify-center">
    <div class="w-1/4 md:mx-auto">
        <div class="text-center">
            <span class="text-grey-dark  text-lg">View Disaster</span>
        </div>
        <div class="bg-white shadow-lg rounded flex flex-col my-2">
            <div class="px-8 pt-6 pb-8 mb-4 "> 
                <div class="-mx-3 mb-0 ">
                    <div class="px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="type">
                            type
                        </label>

                            <!--<input class="appearance-none block w-full bg-grey-lighter text-grey-darker cursor-not-allowed rounded py-3 px-4 mb-3" disabled id="type" type="text" name="type" value="{{ $disaster->type }}">-->

                        <select class="appearance-none block w-full bg-grey-lighter text-grey-darker rounded py-3 px-4 mb-3" name="type" id="type" disabled >
                            <option value="1" @if($disaster->type == '1') selected @endif)>Earthquake</option>
                            <option value="2" @if($disaster->type == '2') selected @endif>Floods</option>
                            <option value="3" @if($disaster->type == '3') selected @endif>Landslide</option>
                            <option value="4" @if($disaster->type == '4') selected @endif>Evacuation Center</option>    
                        </select>                    
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="location">
                                Location
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker cursor-not-allowed border border-grey-lighter rounded py-3 px-4 mb-3" disabled id="location" value="{{  $disaster->loc }}" name="location" type="text">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="longtitude">
                                Longtitude
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  cursor-not-allowed rounded py-3 px-4 mb-3" id="longtitude" disabled type="longtitude" value="{{  $disaster->lng }}" name="longtitude">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                    <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="latitude">
                                Latitude
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border cursor-not-allowed border-grey-lighter rounded py-3 px-4 mb-3" disabled id="latitude" value="{{  $disaster->lat }}" name="latitude" type="text">
                    </div>
                </div>
                <div class="-mx-3 md:flex">

                    @if(Auth::user()->id == 1)
                    <div class="md:w-4/5 px-3 mb-0 md:mb-0">
                        <a href="{{ route('disaster.edit',$disaster->id) }}"class="inline-flex bg-green hover:bg-green-light text-white font-bold py-2 px-4 border-b-4 border-green-dark hover:border-blue rounded">
                            Update
                        </a>
                    </div>
                    <div class="md:w-4/5 px-3 mb-0 md:mb-0">
                        <a href="{{ route('disaster.destroy',$disaster->id) }}"class="inline-flex bg-red hover:bg-red-light text-white font-bold py-2 px-4 border-b-4 border-red-dark hover:border-red rounded" 
                            onclick="event.preventDefault();
                                document.getElementById('destroy-form-{{ $disaster->id}}').submit();">
                            Delete
                        </a>
                        <form id="destroy-form-{{ $disaster->id }}" action="{{ route('disaster.destroy',$disaster->id) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE')}}
                        </form>
                    </div>
                    @endif

                    <div class="md:w-auto px-3">
                        <a href="{{ route('disaster.index') }}"class="inline-flex bg-blue hover:bg-blue-light text-white font-bold py-2 px-4 border-b-4 border-blue-dark hover:border-blue rounded">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection