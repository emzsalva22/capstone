@extends('layouts.app')
@section('title','List All Disaster')
@section('content')
<div class="justify-center">

    <div class="w-4/5 md:mx-auto">
        
        <div class="rounded-none shadow-lg bg-white border-2">
            @if (session('success'))
            <div class="flex bg-green-lighter w-auto">
                    <div class="w-16 bg-green">
                        <div class="p-4">
                            <svg class="h-8 w-8 text-white fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M468.907 214.604c-11.423 0-20.682 9.26-20.682 20.682v20.831c-.031 54.338-21.221 105.412-59.666 143.812-38.417 38.372-89.467 59.5-143.761 59.5h-.12C132.506 459.365 41.3 368.056 41.364 255.883c.031-54.337 21.221-105.411 59.667-143.813 38.417-38.372 89.468-59.5 143.761-59.5h.12c28.672.016 56.49 5.942 82.68 17.611 10.436 4.65 22.659-.041 27.309-10.474 4.648-10.433-.04-22.659-10.474-27.309-31.516-14.043-64.989-21.173-99.492-21.192h-.144c-65.329 0-126.767 25.428-172.993 71.6C25.536 129.014.038 190.473 0 255.861c-.037 65.386 25.389 126.874 71.599 173.136 46.21 46.262 107.668 71.76 173.055 71.798h.144c65.329 0 126.767-25.427 172.993-71.6 46.262-46.209 71.76-107.668 71.798-173.066v-20.842c0-11.423-9.259-20.683-20.682-20.683z"/><path d="M505.942 39.803c-8.077-8.076-21.172-8.076-29.249 0L244.794 271.701l-52.609-52.609c-8.076-8.077-21.172-8.077-29.248 0-8.077 8.077-8.077 21.172 0 29.249l67.234 67.234a20.616 20.616 0 0 0 14.625 6.058 20.618 20.618 0 0 0 14.625-6.058L505.942 69.052c8.077-8.077 8.077-21.172 0-29.249z"/></svg>
                        </div>
                    </div>
                    <div class="w-auto text-grey-darker items-center p-4">
                        <span class="text-lg font-bold pb-4">
                            Heads Up!
                        </span>
                        <p class="leading-tight">
                            {{ session('success') }}
                        </p>
                    </div>
                </div>
            @endif
            <div class="flex bg-grey-lighter">
                <div class="w-full sm:w-full md:w-full xl:w-3/5 lg:w-3/5 p-2">
                    <form action="{{ route('search_all_disaster', ['search' => isset($search) ]) }}" method="GET">
                        <input type="search" class="bg-purple-white shadow rounded border-0 p-3 w-100" placeholder="Search..." name="search">
                        <button class="bg-blue hover:bg-blue-dark text-white font-bold py-3 px-8 rounded">
                            Search
                        </button>
                    </form>
                </div>
                <div class="w-full sm:w-full md:w-full xl:w-2/5 lg:w-2/5 p-2 ">
                    <div class="flex flex-wrap justify-end">
                        <div class="w-auto mr-6">
                            @if(Auth::user()->id == 1)
                            <a class="bg-blue hover:bg-blue-dark text-white font-bold py-3 px-8 rounded" style="display:inline-block;" href="{{ route('disaster.create') }}">
                                Add Disaster
                            </a>
                             @endif
                        </div>
                        <div class="w-auto">
                            <a class="bg-blue hover:bg-blue-dark text-white font-bold py-3 px-8 rounded" style="display:inline-block;" href="{{ route('view_all_disaster') }}">
                                View Disaster(s)
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <table class="border-bottom w-full">
                <thead class="text-red border-solid border-8">
                    <th>ID</th>
                    <th>Type</th>
                    <th>Location</th>
                    <th>Longtitude</th>
                    <th>Latitude</th>
                    <th>Map</th>
                </thead>
                <tbody>
                    @foreach($disasters as $disaster)
                    <tr @if($disaster->id % 2) class="bg-blue-lightest" @endif>
                        <td class="text-center">{{ $disaster->id }}</td>

                        <!--<td class="text-center">{{ $disaster->type }}</td>-->

                        <td class="text-center">
                            @if($disaster->type == '1')
                                Earthquake
                            @elseif($disaster->type == '2')
                                Floods
                            @elseif($disaster->type == '3')
                                Landslides
                            @elseif($disaster->type == '4')
                                Evacuation Areas    
                            @endif
                        </td>
                        <td class="text-center"><a href="{{ route('disaster.show',['id' => $disaster->id]) }}" class="text-blue hover:text-blue-darker">{{ $disaster->loc }}</a></td>
                        <td class="text-center">{{ $disaster->lng }}</td>
                        <td class="text-center">{{ $disaster->lat }}</td>
                        <td class="text-center"><a href="{{ route('view_disaster',$disaster->id) }}" class="text-grey-darkest " target="_blank">
                            <svg class="fill-current text-teal inline-block h-6 w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm2-2.25a8 8 0 0 0 4-2.46V9a2 2 0 0 1-2-2V3.07a7.95 7.95 0 0 0-3-1V3a2 2 0 0 1-2 2v1a2 2 0 0 1-2 2v2h3a2 2 0 0 1 2 2v5.75zm-4 0V15a2 2 0 0 1-2-2v-1h-.5A1.5 1.5 0 0 1 4 10.5V8H2.25A8.01 8.01 0 0 0 8 17.75z"/>
                            </svg>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $disasters->links('vendor.pagination.default') }}
        </div>
    </div>
</div>
@endsection