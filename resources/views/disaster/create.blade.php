@extends('layouts.app')
@section('title','Add Disaster')
@section('content')
<div class="justify-center">
    <div class="w-1/4 md:mx-auto">
        <div class="text-center">
            <span class="text-grey-dark  text-lg">Add Disaster</span>
        </div>
        <div class="bg-white shadow-lg rounded flex flex-col my-2">
            <div class="px-8 pt-6 pb-8 mb-4 "> 
                <form method="POST" action="{{ route('disaster.store') }}">
                    @include('shared.alerts')
                    {{csrf_field()}}
                <div class="-mx-3 mb-0 ">
                    <div class="px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="type">
                            Type
                        </label>

                         <!--<input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3" id="type" type="text" name="type" value="{{ old('type') }}" placeholder="Earthquake">-->

                        <select class="appearance-none block w-full bg-grey-lighter text-grey-darker rounded py-3 px-4 mb-3" name="type" id="type" >
                            <option value="1" @if(old('type') == '1') selected @endif)>Earthquake</option>
                            <option value="2" @if(old('type') == '2') selected @endif>Floods</option>
                            <option value="3" @if(old('type') == '3') selected @endif>Landslide</option>
                            <option value="4" @if(old('type') == '4') selected @endif>Evacuation Center</option>    
                        </select>

                        {{-- <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3" id="type" type="text" name="type" value="{{ old('type') }}" placeholder="Earthquake"> --}}
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="location">
                                Location
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3" id="location" value="{{ old('location') }}" name="location" type="text" placeholder="Goa, Camarines Sur">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                        <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="longtitude">
                                Longitude
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker  rounded py-3 px-4 mb-3" id="longtitude" type="longtitude" value="{{ old('longtitude') }}" name="longtitude">
                    </div>
                </div>
                <div class="-mx-3 mb-0 ">
                    <div class=" px-3 mb-6 md:mb-0">
                        <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="latitude">
                                Latitude
                            </label>
                        <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 mb-3" id="latitude" value="{{ old('latitude') }}" name="latitude" type="text">
                    </div>
                </div>
                <div class="-mx-3 md:flex">
                    <div class="md:w-4/5 px-3 mb-0 md:mb-0">
                        <button class="bg-blue hover:bg-blue-light text-white font-bold py-2 px-4 border-b-4 border-blue-dark hover:border-blue rounded">
                            Save
                        </button>
                    </div>
                    <div class="md:w-auto px-3">
                        <a href="{{ route('disaster.index') }}"class="inline-flex bg-red hover:bg-red-light text-white font-bold py-2 px-4 border-b-4 border-red-dark hover:border-blue rounded">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection