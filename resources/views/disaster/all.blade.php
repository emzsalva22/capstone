@extends('layouts.app')
@section('title','View Disaster')
@section('content')
<div class="justify-center">
    <div class="flex flex-row bg-grey-lighter border border-2 shadow-lg w-5/6 mx-auto">
        <div class="text-grey-darker text-center  px-4 py-2 m-2 w-4/5">
            <div id="map" style="height: 50em;"></div>
        </div>
        <div class="text-grey-darker   px-4 py-2 m-2 w-auto">
            <div class="flex justify-between py-1">
                {{-- <h1 class="text-center block">Disaster(s)</h3> --}}
                <a href="javascript:void(0)" class="bg-transparent text-blue-dark font-semibold py-2 px-4 rounded">Legend</a>
            </div>
                <div class="w-full">
                    <ul class="list-reset">
                        <li class="mb-4 mt-4">
                            <p>                            
                                <img src="http://maps.google.com/mapfiles/ms/micons/earthquake.png" alt="earthquake" class="src" style="vertical-align:middle">
                                Earthquake
                            </p>
                        </li>
                        <li class="mb-4">
                            <p>                            
                                <img src="{{ asset('img/floods.png')}}" alt="earthquake" class="src" style="vertical-align:middle">
                                Floods
                            </p>
                        </li>
                        <li class="mb-4">
                            <p>                            
                                <img src="{{ asset('landslide.png')}}" alt="earthquake" class="src" style="vertical-align:middle">
                                Landslide
                            </p>
                        </li>
                        <li class="mb-4">
                            <p>                            
                                <img src="http://maps.google.com/mapfiles/ms/icons/hospitals.png" alt="earthquake" class="src" style="vertical-align:middle">
                                Evacuation Center
                            </p>
                        </li>
                        
                        <li class="mb-4">
                            <p>                            
                                <img src="https://maps.google.com/mapfiles/ms/micons/man.png" alt="earthquake" class="src" style="vertical-align:middle">
                                User
                            </p>
                        </li>
                    </ul>
                    {{-- <ol>
                        @foreach($disasters as $disaster)
                        <li>{{ $disaster->loc }}</li> 
                        <ul>
                            <li>Lat: {{ $disaster->lat }}</li>
                            <li>Long: {{ $disaster->lng }}</li>
                        </ul>         
                        @endforeach
                    </ol> --}}
                </div>
            </div>
               
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3Y9f16nxJ1Hjgoxdjh9vfWh3u_gDH4ok&callback=initMap">
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 13,
         center: { lat: 13.7362, lng: 123.4159 }
    });

    setMarkers(map);
}

var beaches = [
    @foreach($disasters as $disaster)
        [
            '{{ $disaster->loc }}', {{ $disaster->lat }}, {{ $disaster->lng }}, {{ $disaster->type }} 
        ],
        @if($loop->last)
        @endif
    @endforeach
];

var image = [
    'http://maps.google.com/mapfiles/ms/micons/earthquake.png',
    '{{ asset('img/floods.png')}}',
    //'http://maps.google.com/mapfiles/ms/icons/waterfalls.png',
    '{{ asset('landslide.png')}}',
    'http://maps.google.com/mapfiles/ms/icons/hospitals.png',
    'https://maps.google.com/mapfiles/ms/micons/man.png'
];

var shape = {
    coords: [1, 1, 1, 20, 18, 20, 18, 1],
    type: 'poly'
};

function setMarkers(map) {
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        new google.maps.Marker({
            position: { lat: position.coords.latitude, lng: position.coords.longitude},
            map:map,
            icon:image[4]
        });
        var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        map.setCenter(pos);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
    for (var i = 0; i < beaches.length; i++) {
        var beach = beaches[i];
        console.log(beach[3]);
        navigator.geolocation.getCurrentPosition(function(position){
            if(beach[3] != 4) {
                console.log(getEventType(beach[3]));
                distance(beach[1],beach[2],position.coords.latitude,position.coords.longitude,beach[0], getEventType(beach[3]) );
            }
        });
        switch(beach[3]) {
            case 1:
                var marker = new google.maps.Marker({
                    position: {lat: beach[1], lng: beach[2]},
                    map: map,
                    icon: image[0],
                    shape: shape,
                    // title: beach[0],
                    zIndex: beach[3]
                });
                break;
            case 2: 
                var marker = new google.maps.Marker({
                    position: {lat: beach[1], lng: beach[2]},
                    map: map,
                    icon: image[1],
                    // shape: shape,
                    // title: beach[0],
                    zIndex: beach[3]
                });
                break;
            case 3:
                var marker = new google.maps.Marker({
                    position: {lat: beach[1], lng: beach[2]},
                    map: map,
                    icon: image[2],
                    // shape: shape,
                    // title: beach[0],
                    zIndex: beach[3]
                });
                break;
            case 4:
                var marker = new google.maps.Marker({
                    position: {lat: beach[1], lng: beach[2]},
                    map: map,
                    icon: image[3],
                    // shape: shape,
                    // title: beach[0],
                    zIndex: beach[3]
                });
                break;
        }
    }
}

function getEventType(event)
{
    switch(event) {
        case 1:
            return 'Earthquake';
            break;
        case 2: 
            return 'Floods';
            break;
        case 3:
            return 'Landslide';
            break;
            
    }

}

function distance(lat1, lon1, lat2, lon2,loc,event) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var radlon1 = Math.PI * lon1/180
    var radlon2 = Math.PI * lon2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    dist = dist * 1.609344;
    if(dist < 15) {
        console.log(dist);
        callSwal(loc,event);
    }
    return dist
}
function callSwal(loc,disaster)
{
    swal({
        title: loc + ' - ' + disaster,
        text: "You have been warned!",
        icon: "warning",
        button: "Evacuate",
    });
}
</script>
@endpush