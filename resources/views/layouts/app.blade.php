<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body class="bg-grey-lightest h-screen border-t-8 border-blue-light">
    <div id="app">
        @auth
        <nav class="shadow flex items-center justify-between flex-wrap bg-grey-lightest p-6 mb-6">
            
            <div class="flex items-center flex-no-shrink text-grey-darkest mr-6">
                <span class="font-semibold text-xl tracking-tight">                       
                    {{ config('app.name', 'Laravel') }}
                </span>
            </div>
            <div class="block lg:hidden">
                <button class="flex items-center px-3 py-2 border rounded text-teal-lighter border-teal-light hover:text-white hover:border-white">
                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
                </button>
            </div>
            <div class="block sm:hidden w-full flex-grow lg:flex lg:items-center lg:w-auto ">
                <div class="text-sm lg:flex-grow">
                    <a href="{{ route('home') }}" class="block mt-4 lg:inline-block lg:mt-0 text-teal-darkest hover:text-grey mr-4">
                        Home
                    </a>
                     <a href="{{ route('disaster.index') }}" class="block mt-4 lg:inline-block lg:mt-0 text-teal-darkest hover:text-grey mr-4">
                        Disaster
                    </a>
                    @if(Auth::user()->id == 1)
                    <a href="{{ route('user.index') }}" class="block mt-4 lg:inline-block lg:mt-0 text-teal-darkest hover:text-grey">
                        Users
                    </a>
                    @endif
                </div>
                <div>
                @guest
                    <a class="no-underline hover:underline text-grey-darker pr-3 text-sm" href="{{ url('/login') }}">Login</a>
                    <a class="no-underline hover:underline text-grey-darker text-sm" href="{{ url('/register') }}">Register</a>
                @else
                <a  class="no-underline hover:underline text-teal-darkest text-sm" href="{{ route('user.profile',Auth::user()->id) }}">My Account</a>
                    <a href="{{ route('logout') }}"
                        class="no-underline hover:underline text-teal-darkest text-sm"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endguest
                </div>
            </div>
        </nav>
        @endauth

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <style>
        a{
            text-decoration: none;
        }
    </style>
    @stack('scripts')
</body>
</html>
