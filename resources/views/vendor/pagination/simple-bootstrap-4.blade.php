@if ($paginator->hasPages())
    <ul class="flex list-reset border border-grey-light rounded w-auto font-sans">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="block hover:text-white hover:bg-blue text-blue border-r border-grey-light px-3 py-2 disabled"><span class="page-link">@lang('pagination.previous')</span></li>
        @else
            <li class="block hover:text-white hover:bg-blue text-blue border-r border-grey-light px-3 py-2"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="block hover:text-white hover:bg-blue text-blue border-r border-grey-light px-3 py-2"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>
        @else
            <li class="block hover:text-white hover:bg-blue text-blue border-r border-grey-light px-3 py-2 disabled"><span class="page-link">@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
