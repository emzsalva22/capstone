@if ($paginator->hasPages())
    <ul class="flex list-reset border border-grey-light rounded w-auto font-sans">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="block text-blue px-3 py-2 rounded opacity-50 cursor-not-allowed"><span>&rsaquo;</span></li>
        @else
            <li class="block hover:text-white hover:bg-blue text-blue px-3 py-2"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&lsaquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="block hover:text-white hover:bg-blue text-blue px-3 py-2 disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="block text-white bg-blue border-r border-blue px-3 py-2"><span>{{ $page }}</span></li>
                    @else
                        <li class="block hover:text-white hover:bg-blue text-blue px-3 py-2"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="block hover:text-white hover:bg-blue text-blue px-3 py-2" ><a href="{{ $paginator->nextPageUrl() }}" rel="next">&rsaquo;</a></li>
        @else
            <li class="block text-blue px-3 py-2 rounded opacity-50 cursor-not-allowed"><span>&rsaquo;</span></li>
        @endif
    </ul>
@endif


