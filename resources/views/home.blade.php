@extends('layouts.app')
@section('content')

<div class="flex items-center">
    <div class="md:w-1/2 md:mx-auto">
          <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script>
        new WOW().init();
    </script>
 
        <div class="rounded shadow">
            <div class="font-medium text-lg text-black bg-grey-light p-3 rounded-t" style="text-align: center;">
                Disaster Risk Management System
            </div>
            <!--<div class="bg-white p-3 rounded-b">
                @if (session('status'))
                    <div class="bg-green-lightest border border-green-light text-green-dark text-sm px-4 py-3 rounded mb-4">
                        {{ session('status') }}
                    </div>
                @endif
                    <p class="text-grey-dark text-sm">
                    Welcome to Disaster Map!
                    </p>-->
            </div>
        </div>

     

             <!--<div>
                 <img src="logo2.png" style="width: 23%; background:#d64e3b; background:#4ea9f4; height: 23%; margin-top: 1%; margin-left: -48%; #aeccfc">
            </div>-->
 </div>
</div>

<!--
<center>
<div style="width: 1300px; height: 470px; margin-top: 20px;border-radius: 10px 10px 10px 10px; border: 5px solid #4ea9f4">

   <div class="system1 wow fadeInUp animated";  style="width: 300px; height: 400px; border: 3px solid #4ea9f4; border-radius: 10px 10px 10px 10px; margin-left: 8%; margin-top: 3%; float: left; overflow: scroll; overflow-x: hidden; font-family: century gothic; line-height: 1.6; text-align: justify; box-shadow: 6px 6px #aeccfc; padding: 15px 15px;">
        <center><strong>ABOUT THE SYSTEM</strong></center><br>
        The title of the system is cloud-based map information and risk awareness system in Goa and San Jose Camarines Sur. The main function of the system is to give an awareness and to help the citizens of Goa and San Jose Camarines Sur in terms of calamity. This system is subliminally unique because unlike Google maps that search places, it has an additional module for risk awareness management. With that, it can be a great help for the LGU (Local Government Unit) of Goa and San Jose to keep track of all the disaster prone areas.
        <br><br>
        The developers create the system using the software and hardware specifications. On the software requirements the developers used Windows and 10 for Operating System, Google Chrome at least version 63 and Mozilla Firefox Version 63.0.3 for Web Browser, Sublime Text 3 for Code Editor, Oncloud for Cloud-Database, Laragon localhost Server Version 3.2.3 for Back up Database, and Adobe photoshop for image editing. While on the hardware requirements the developers used Intel Core i5 2.50GHz of Processor, 4.00GB of RAM, 64 Bit system type, 100.00 GB free space in Hard Disk, and at least 2.00 GB Graphics Processor.

    </div>



    
     <div class="disaster1 wow fadeInDown animated";  style="width: 300px; height: 400px; margin-top: 3%; margin-left: 9.5%; border: 3px solid #4ea9f4; border-radius: 10px 10px 10px 10px; float: left; overflow: scroll; overflow-x: hidden; font-family: century gothic; line-height: 1.6; text-align: justify; box-shadow: 6px 6px #aeccfc; padding: 15px 15px;">
        <center><strong>ABOUT DISASTER</strong></center><br>
        A disaster is a serious disruption, occurring over a relatively short time, of the functioning of a community or a society involving widespread human, material, economic or environmental loss and impacts, which exceeds the ability of the affected community or society to cope using its own resources.In contemporary academia, disasters are seen as the consequence of inappropriately managed risk. These risks are the product of a combination of both hazards and vulnerability. Hazards that strike in areas with low vulnerability will never become disasters, as in the case of uninhabited regions.

    </div>

   
     <div class="dev1 wow fadeInUp animated"; style="width: 300px; height: 400px; border: 3px solid #4ea9f4; margin-top: 3%; margin-left: 10%; border-radius: 10px 10px 10px 10px; float: left; overflow: scroll; overflow-x: hidden; font-family: century gothic; line-height: 1.6; text-align: justify; box-shadow: 6px 6px #aeccfc; padding: 15px 15px;">
          <center><strong>HOW TO USE</strong></center><br>
          The system works by running the localhost server, in this project, laragon is used as the localhost. then, go to your default web browser but we recommend to use Mozilla Firefox version 63 for more accurate results.<br><br>To add disaster data, simply click the "Disaster" located at the upper part of your screen, from there, click the "Add Disaster" button and simply fill in the required details that is being ask. you can also view all the disasters that is being list up in the table by simple clicking the "View Disasater(s)" button. The system also tracks your real-time location and alerts you if you're within the range of the disaster prone areas. You can update the disaster data by simply clicking on the disaster location and change any necessary data you want to change. Deleting disaster data is also located in the updating module, you can delete data by simple clicking the delete button. You can also view all the users that is registered into the system to keep track of who can use the system. All this function is active if you're an admin of the system while if you're a regular user of the system, you can just view disaster data, but still it is a big help.

    </div>

</div>
</center>
-->

<!--option 2-->

    <!--<div style="width:40px; height: 40px;"> <img src="img/e1.png" style="width:60px; height: 60px; margin-top: 6px; margin-left: 458px;"> 
    </div> dae ni kasali

    <div style="width:40px; height: 40px;"> <img src="img/e1.png" style="width:60px; height: 60px; margin-top: 5px; margin-left: 580px;"> 
    </div>

    <div style="width:40px; height: 40px;"> <img src="img/e1.png" style="width:60px; height: 60px; margin-top: 5px; margin-left: 700px;"> 
    </div>

    <div style="width:40px; height: 40px;"> <img src="img/e1.png" style="width:60px; height: 60px; margin-top: 5px; margin-left: 900px;"> 
    </div>-->


    
   <!--<div style="background: #4CAF50; color: white; padding: 15px; width: 50%;height: 100px;overflow: scroll;border: 1px solid #ccc;">This text is really long and the height of its container is only 100 pixels. Therefore, a scrollbar is added to help the reader to scroll the content. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem</div>-->

<!--<!DOCTYPE html>
<html>
<head>
    <title>Content</title>
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <script>
        new WOW().init();
    </script>
 
</head>
<style type="text/css">


    /*pag dito ko nilagay di gumagana ung style*/
</style>

<body>

    <div id="system" class="container1 fadeInDown animated" style=" width: 100%; height: 600px; position: fixed;top: 85px; background:#f7f7f7; border-radius: 15px 15px 15px 15px ; border: 4px solid #4ea9f4;" >
        <h2 style="text-align: center;margin-top: 10px; margin-bottom: 10px;margin-left: 10px;font-family: century-gothic; font-size: 37px;">About The System</h2> <br>

        <div style="float: left; margin-left: 9%;">
        <p style="text-align: justify; margin-left: 10%;margin-right: 10%; font-size: 20px; font-family: century-gothic; width: 300px; height: 300px;">The purpose of this system is to give an awareness and to help citizens of  Goa and San Jose Camarines Sur in terms of calamity. This system is subliminally unique because unlike Google maps that search places, it has an additional module for risk awareness management.  </p><br>
        </div>

        <div style="float: left; margin-left: 6%;">
        <p style="text-align: justify; margin-left: 10%;margin-right: 10%; font-size: 20px; font-family: century-gothic; width: 300px; height: 300px;">The purpose of this system is to give an awareness and to help citizens of Goa and San Jose Camarines Sur in terms of calamity. This system is subliminally unique because unlike Google maps that search places, it has an additional module for risk awareness management. </p>
        </div>

          <div style="float: left; margin-left: 5%;">
        <p style="text-align: justify; margin-left: 10%;margin-right: 10%; font-size: 20px; font-family: century-gothic; width: 300px; height: 300px;">The purpose of this system is to give an awareness and to help citizens of Goa and San Jose Camarines Sur in terms of calamity. This system is subliminally unique because unlike Google maps that search places, it has an additional module for risk awareness management. </p>
        </div>
    </div>
    

    <div id="contents" class="container2 wow fadeInUp animated"; data-wow-delay="1s"; style="text-align: left; width: 100%;position: relative;top: 700px; background:#f7f7f7;height: 1500px;border-radius: 15px 15px 15px 15px ; border: 4px solid #d64e3b;">
        <h2 style="text-align: center;margin-top: 15px;margin-bottom: 10px; font-family: century-gothic; font-size: 37px;">About Disaster</h2><br>

        <p class="p1-d wow fadeInLeft animated"; data-wow-delay="1s"; style="text-align: justify; margin-left: 10%;margin-right: 10%; font-size: 20px; font-family: century-gothic;">A disaster is a serious disruption, occurring over a relatively short time, of the functioning of a community or a society involving widespread human, material, economic or environmental loss and impacts, which exceeds the ability of the affected community or society to cope using its own resources.In contemporary academia, disasters are seen as the consequence of inappropriately managed risk. These risks are the product of a combination of both hazards and vulnerability. Hazards that strike in areas with low vulnerability will never become disasters, as in the case of uninhabited regions.(Wikipedia)</p>

        <p class="p2-d wow fadeInLeft animated"; data-wow-delay="1s"; style="text-align: justify; margin-left: 10%;margin-right: 10%; font-size: 20px; font-family: century-gothic; margin-top: 30px;">There are two types of disaster, in this study, the researchers focused on the natural disaster. A natural disaster is a natural process or phenomenon that may cause loss of life, injury or other health impacts, property damage, loss of livelihoods and services, social and economic disruption, or environmental damage.Various phenomena like earthquakes, landslides, volcanic eruptions, floods, hurricanes, tornadoes, blizzards, tsunamis, and cyclones are all natural hazards that kill thousands of people and destroy billions of dollars of habitat and property each year. However, the rapid growth of the world's population and its increased concentration often in hazardous environments has escalated both the frequency and severity of disasters. With the tropical climate and unstable landforms, coupled with deforestation, unplanned growth proliferation, non-engineered constructions make the disaster-prone areas more vulnerable. Developing countries suffer more or less chronically from natural disasters due to ineffective communication combined with insufficient budgetary allocation for disaster prevention and management. Asia tops the list of casualties caused by natural hazards. The following images are the effects of natural disaster. Credits to the owner of this images.</p>
        
        <div class="image1 wow bounceInUp animated"; data-wow-delay="1s"; style="margin-top: 20px;  float: left; margin-left: 55px;">
        <img  src="img/image3.jpg" style="width: 300px; height: 300px;">
        </div>
        <div class="image2 wow bounceInUp animated"; data-wow-delay="1s" style="margin-top: 20px; margin-left: 10px; float: left;">
        <img src="img/image4.jpg" style="width: 310px; height: 300px;">
        </div>
        <div class="image3 wow bounceInUp animated"; data-wow-delay="1s" style="margin-top: 20px; margin-left: 10px;float: left;">
        <img src="img/image5.jpg" style="width: 300px; height: 300px;">
        </div>
        <div class="image4 wow bounceInUp animated"; data-wow-delay="1s" style=" margin-top: 20px; float: left; margin-left: 10px">
        <img src="img/image9.jpg" style="width: 300px; height: 300px;">
        </div>


        <button onclick="topFunction()" id="myBtn" title="Go to top" style="display: none;position: fixed;bottom: 20px;right: 30px; z-index: 99;font-size: 15px; font-family: century-gothic; border: none;outline: none;background-color: #18a522;color: black;cursor: pointer; padding: 15px;border-radius: 4px solid #0c5b2c;">TOP</button>
    </div>

    <script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
</body>

</html>-->
            
@endsection

