@extends('layouts.app')
@section('title','View Disaster')
@section('content')
<div class="justify-center">
    <div class="flex flex-row bg-grey-lighter border border-2 shadow-lg w-5/6 mx-auto">
        <div class="text-grey-darker text-center  px-4 py-2 m-2 w-4/5">
            <div id="map" style="height: 50em;"></div>
        </div>
        <div class="text-grey-darker text-center  px-4 py-2 m-2 w-auto">
            <div class="flex justify-between py-1">
                <h3 class="text-sm">Disaster #{{ $disaster->id }}</h3>
            </div>
            <div class="text-sm mt-2">
                <div class="bg-white p-2 rounded mt-1 border-b border-grey cursor-pointer hover:bg-grey-lighter">
                    Location: {{ $disaster->loc }}
                </div>

                <!--<div class="bg-white p-2 rounded mt-1 border-b border-grey cursor-pointer hover:bg-grey-lighter">
                    Type: {{ $disaster->type }}
                </div>-->

                <div class="bg-white p-2 rounded mt-1 border-b border-grey cursor-pointer hover:bg-grey-lighter">
                    @if($disaster->type == '1')
                        Type: Earthquake
                    @elseif($disaster->type == '2')
                        Type: Floods
                    @elseif($disaster->type == '3')
                        Type: Landslides
                    @elseif($disaster->type == '4')
                        Type: Evacuation Areas
                    @endif
                </div>
                <div class="bg-white p-2 rounded mt-1 border-b border-grey cursor-pointer hover:bg-grey-lighter">
                    Longtitude: {{ $disaster->lng }}
                </div>
                <div class="bg-white p-2 rounded mt-1 border-b border-grey cursor-pointer hover:bg-grey-lighter">
                    Latitude: {{ $disaster->lat }}
                </div>
                <div class="flex items-center justify-between mt-1">
                    <a href="{{ route('view_prev_disaster',$disaster->id) }}" class="bg-white hover:bg-grey-lighter  py-2 px-4  border-b border-grey cursor-pointer">
                        <svg class="fill-current text-teal inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><polygon points="3.828 9 9.899 2.929 8.485 1.515 0 10 .707 10.707 8.485 18.485 9.899 17.071 3.828 11 20 11 20 9 3.828 9"/></svg>
                    </a>
                    <a href="{{ route('view_next_disaster',$disaster->id) }}" class="bg-white hover:bg-grey-lighter  py-2 px-4  border-b border-grey cursor-pointer">
                        <svg class="fill-current text-teal inline-block h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><polygon points="16.172 9 10.101 2.929 11.515 1.515 20 10 19.293 10.707 11.515 18.485 10.101 17.071 16.172 11 0 11 0 9"/></svg>                  
                    </a>
                </div>
               
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
    var map;
    var myLatLng = {lat: {{ $disaster->lat }}, lng: {{ $disaster->lng }} };
    function loadMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
         zoom: 12
        });
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          animation: google.maps.Animation.DROP,
          title: '{{ $disaster->loc }}'
        });
    }
    
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0bX87ybq-_gQklGiq1-fS92hy-tmsBjE&callback=loadMap" async defer>
</script>
@endpush