<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Disaster;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function view_disaster($id)
    {
        return view('map.index')->with(['disaster' => Disaster::find($id)]);
    }
    
    public function prev_disaster($id)
    {
        $prev_id = Disaster::where('id', '<', $id)->max('id');
        if ($prev_id) {
            $disaster = Disaster::find($prev_id);
            return view('map.index')->with(['disaster' =>  $disaster]);
        } else {
            $disaster = Disaster::find($id);
            return view('map.index')->with(['disaster' =>  $disaster]);
        }
    }

    public function next_disaster($id)
    {
        $next_id = Disaster::where('id', '>', $id)->min('id');
        if ($next_id) {
            $disaster = Disaster::find($next_id);
            return view('map.index')->with(['disaster' =>  $disaster]);
        } else {
            $disaster = Disaster::find($id);
            return view('map.index')->with(['disaster' =>  $disaster]);
        }
    }

}
