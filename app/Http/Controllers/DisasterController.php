<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Disaster;
use PulkitJalan\GeoIP\GeoIP;

class DisasterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('disaster.index')->with(['disasters' => Disaster::paginate(20) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('disaster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'type' => 'required',
            'location' => 'required',
            'longtitude' => 'required',
            'latitude' => 'required',
        ]);
        Disaster::create([
            'type' => $request->type,
            'loc' => $request->location,
            'lng' => $request->longtitude,
            'lat' => $request->latitude
        ]);
        return redirect()->back()->with([
            'success' => 'Disaster added!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('disaster.view')->with(['disaster' => Disaster::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('disaster.edit')->with(['disaster' => Disaster::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'type' => 'required',
            'location' => 'required',
            'longtitude' => 'required',
            'latitude' => 'required',
        ]);
        $disaster = Disaster::find($id);
        $disaster->type = $request->type; 
        $disaster->loc = $request->location;
        $disaster->lng = $request->longtitude; 
        $disaster->lat = $request->latitude;
        $disaster->save();
        return redirect()->back()->with([
            'success' => 'Disaster updated!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disaster = Disaster::find($id)->delete();
        return redirect()->route('disaster.index')->with([
            'success' => 'Record removed!'
        ]);
    }

    public function view_all_disaster()
    {
        // dd(\Request::getClientIp());
        // $geoip = new GeoIP();
        // dd($geoip->getLatitude());
        // dd(GeoIP::getLocation('27.974.399.6'));
        $disasters = Disaster::all();
        // dd($disasters);
        return view('disaster.all')->with([
            'disasters' => $disasters,
        ]);
    }

    public function demo()
    {
        return view('demo');
    }

    public function search(Request $request) 
    {
        // $disasters = Disaster::where('loc','like','%'.$request->search.'%')->orWhere('type','like','%'.$request->search.'%')->paginate(5);
        $disasters = Disaster::where('loc','like','%'.$request->search.'%')->paginate(5);
        return view('disaster.index')->with(['disasters' => $disasters ]);
    }
}
