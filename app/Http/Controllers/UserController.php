<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_user()
    {
        $user = Auth::user()->id;
        // dd($user);
        return $user    == '1' ? true : false;
    }

    public function is_user_valid($view)
    {
        return $this->get_user() ? $view : error(404);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = view('user.index')->with([ 'users' => User::where('id','!=',Auth::user()->id)->paginate(30) ]);
        return $this->is_user_valid($view);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'firstname' => 'required',  
            'middlename' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|confirmed'
        ]);
        User::create([
            'firstname' => $request->firstname,
            'middlename' => $request->middlename,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => $request->password
        ]);
        return redirect()->back()->with([
            'success' => 'User created!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('user.show')->with([ 'user' => User::find($id )]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.edit')->with([ 'user' => User::find($id) ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test = $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'middlename' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'nullable|min:6'
        ]);
        $user = User::find($id);
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        if ($request->password) {
           $user->password = $request->password; 
        }
        $user->save();
        return redirect()->back()->with([
            'success' => 'User updated!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disaster = User::find($id)->delete();
        return redirect()->route('user.index')->with([
            'success' => 'Record removed!'
        ]);
    }
    
    public function profile($id)
    {
        return view('user.profile')->with([ 'user' => User::find($id) ]);
    }
    public function edit_profile($id)
    {
        return view('user.update')->with([ 'user' => User::find($id) ]);

    }

    public function search(Request $request) 
    {
        // $disasters = Disaster::where('loc','like','%'.$request->search.'%')->orWhere('type','like','%'.$request->search.'%')->paginate(5);

        $users = User::where('firstname','like','%'.$request->search.'%')->paginate(5);

        //$users = User::where('firstname','like','%'.$request->search.'%')
                    //->orWhere('middlename','like','%'.$request->search.'%')
                    //->orWhere('lastname','like','%'.$request->search.'%')
                    //->orWhere('email','like','%'.$request->search.'%')
                    //->paginate(5);
                     //($users);
        //$view = view('user.index')->with([ 'users' => User::where('id','!=',Auth::user()->id)->paginate(30) ]);
        //return $this->is_user_valid($view);
         $view = view('user.index')->with([ 'users' => $users ]);
         return $this->is_user_valid($view);
    }

}
